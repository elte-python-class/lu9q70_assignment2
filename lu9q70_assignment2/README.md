# TASK 1
## PART 3

```
d1 = [A,T,C,G]
d2 = [T,A,G,C]
d1*d2
```
The above code should work and give __TRUE__ as output since these two DNA strings are complementary.
Similarly,
```
d1+d2
```
... will most probably return ```[A,T,C,G,T,A,G,C]``` as output.

## PART 4

I have provided a file called ```Sample.fasta``` in the repository.

### Supplying a Filename:
The code should be ```python3 task1.py --filename Sample.fasta``` which will print the length of the DNA. If you wish to use a file of your own, make sure to put it in the same directory as the ```task1.py``` file and in your ```PYTHONPATH```. For input as strings, see below:

### Supplying a String Input:
The code should be ```python3 task1.py --string ```(your string value i.e.ATGCATGATAGAG). This will print the length of the DNA. If you provide a string in lowercase or letters except nucleobase codes (A, G, C or T) it will give errors. You can check. :)

### Molecular Weight:
If you also wish to know the molecular weight of your DNA (provided either by __a string or a filename__), you will have to enter ```--verbose``` or ```--v``` at the end of the code. For example, the code could be ```python3 task1.py --string ATGATAGATCCATCATGAGATGTTA --v```.

# TASK 2

Incomplete. I could not reproduce the graph and I did not want to cheat either. I did take help and look at my classfellows' codes, but what you see in the (although) incomplete code, I understand it.

# TASK 3

Completed.

# TASK 4

Completed.

# +1

Amidst the exam period and my __underestimating the time to complete the assignment__ or just __procrastination__, I could not find time to work on the homework and previous assignment. I hope you can excuse me for that.
