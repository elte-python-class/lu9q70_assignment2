import pymongo
client = pymongo.MongoClient('localhost:27017')
db=client["python2122"]

albums=db["albums"]
artists=db["artists"]

ALBUMS=list(db.albums.find())
ARTISTS=list(db.artists.find())

final=[]
for x in ALBUMS:
    for y in x["tracks"]:
        if 90000 <= y["milliseconds"] <= 120000:
            for i in ARTISTS:
                ID=i["_id"]
                if ID==x["artist_id"]:
                    NAME=i["name"]
            final.append({"title":y["name"],"composer":y["composer"],
                         "artist":NAME,"album":x["title"]})

print(final)
print(len(final))
            