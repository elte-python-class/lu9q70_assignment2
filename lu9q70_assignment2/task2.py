import pandas as pd
import numpy as np
import locale
from datetime import datetime
import re

df=pd.read_csv("BabyRecords.csv")
Interesting_Categories=["Feeding","Pumping"]

dfnew=df[df.RecordCategory.isin(Interesting_Categories)]
mydict={"aug.":"08","szept.":"09","okt.":"10","nov.":"11","dec.":"12"}
dfnew['StartDate']=pd.to_datetime(dfnew['StartDate'].replace(mydict,regex=True))
dfnew['FinishDate']=pd.to_datetime(dfnew['FinishDate'].replace(mydict,regex=True))

dfnew1=dfnew.replace(to_replace=r'\,+ \w+ \w+',value='',regex=True)
dfnew2=dfnew1.replace(to_replace=r'\,+ \w+',value='',regex=True)
dfnew3=dfnew2.replace(to_replace=np.nan,value='0ml',regex=True)
dfnew4=dfnew3.drop(columns=["RecordSubCategory","FinishDate"])
dfnew5=dfnew4.groupby(["RecordCategory","StartDate"]).sum()
