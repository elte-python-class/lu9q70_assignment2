import argparse


class Nucleobase:
    ndic1 = {"A": (135.13, "thymine", "adenine"), "G": (151.13, "cytosine", "guanine"),
             "C": (111.1, "guanine", "cytosine"), "T": (126.1133, "adenine", "thymine")}
    ndic2 = {"adenine": (135.13, "thymine", "A"), "guanine": (151.13, "cytosine", "G"),
             "cytosine": (111.1, "guanine", "C"), "thymine": (126.1133, "adenine", "T")}

    def __init__(self, entry):
        if len(entry) == 1:
            self.code = entry
            if entry not in self.ndic1.keys():
                print(
                    "Please ensure that nucleotide base code(s) is/are in proper format. (A/C/G/T)")
            self.molecular_weight = self.ndic1[self.code][0]
            self.complementer = self.ndic1[self.code][1]
            self.name = self.ndic1[self.code][2]
        else:
            self.name = entry.lower()
            if entry.lower() not in self.ndic2.keys():
                print(
                    "Please ensure that nucleotide labels and codes are properly entered. (Adenine/Guanine/Cytosine/Thymine)")
            self.molecular_weight = self.ndic2[self.name.lower()][0]
            self.complementer = self.ndic2[self.name.lower()][1]
            self.code = self.ndic2[self.name.lower()][2]

    def __lt__(self, other):
        return((self.molecular_weight) < (other.molecular_weight))

    def complement(self):
        self.name = self.complementer
        self.molecular_weight = self.ndic2[self.name.lower()][0]
        self.complementer = self.ndic2[self.name.lower()][1]
        self.code = self.ndic2[self.name.lower()][2]
        return self.code

    def __repr__(self):
        return self.code

    def __mul__(self, other):
        return self.name == other.complementer

    def __eq__(self, other):
        return self.name == other.name

    def __add__(self, other):
        return self.code + other.code


class DNA(list):
    def __init__(self, entry2):
        self.entry2 = entry2
        if type(entry2) == str:
            mylist = []
            for n1 in entry2:
                mylist.append(Nucleobase(n1))

        elif type(entry2) == list:
            mylist = []
            for n2 in entry2:
                if type(n2) == str:
                    mylist.append(Nucleobase(n2))
                elif type(n2) == Nucleobase:
                    mylist.append(n2)
        else:
            raise NotImplementedError()
        self.mylist = mylist

    def print(self):
        nblist = ''
        for num in self.entry2:
            nblist = nblist+Nucleobase(num).code
        return nblist

    def molecular_weight(self):
        total_mw = 0
        for num in self.entry2:
            total_mw = total_mw+Nucleobase(num).molecular_weight
        return total_mw

    def complement(self):
        complist = ''
        for num in self.entry2:
            complist = complist+Nucleobase(num).complement()
        return complist

    def __add__(self, other):
        return self.mylist + other.mylist

    def __mul__(self, other):
        otherlist = []
        for n in self.mylist:
            otherlist.append(Nucleobase(n.complementer))

        return other.mylist == otherlist


p = argparse.ArgumentParser()
p.add_argument('--verbose', action='store_true')
p.add_argument('--filename', type=str, default=None)
p.add_argument('--string', type=str, default=None)
args = p.parse_args()

byfile = ''
bystring = ''
if (args.filename is not None) and (args.string is not None):
    print("Both filenames and strings cannot be given simultaneously.")

elif (args.filename is None) and (args.string is None):
    print("Please provide either a filename or a nucleotide sequence to work with.")

elif args.filename is not None:
    with open(args.filename, 'r') as f:
        lines = f.readlines()
        lines = lines[1:]
        print("The length of DNA is: (bases)")
        string = ''
        string = string.join(lines).replace('\n', "")
        print(len(string))

    if args.verbose == True:
        print("The molecular weight is: (g/mol)")
        print(DNA(string).molecular_weight())


elif args.string is not None:
    bystring = DNA(args.string).print()
    print("The length of DNA is: (bases)")
    print(len(bystring))
    if args.verbose == True:
        print("The molecular weight is: (g/mol)")
        print(DNA(bystring).molecular_weight())
