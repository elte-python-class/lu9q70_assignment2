import pandas as pd
import sqlite3

conn=sqlite3.connect('chinook.db')
curs=conn.cursor()

mydf=pd.read_sql(
"""
SELECT tracks.Name as TrackName,tracks.Composer,albums.Title as AlbumTitle,Artists.Name as Artist
FROM ((albums
INNER JOIN tracks
ON tracks.AlbumId = albums.AlbumId)
INNER JOIN artists
ON artists.ArtistId=albums.ArtistId)
WHERE MilliSeconds BETWEEN 90000 AND 120000
ORDER BY Composer;
""",conn)

print(mydf)